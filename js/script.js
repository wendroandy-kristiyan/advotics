$(document).ready(function () {
  var start = moment().subtract(7, 'days');
  var end = moment().subtract(1, 'days');
  var label = "";
  var chart;


  //initiate daterange
  $('.period .interval').daterangepicker({
    "maxSpan": {
      "month": 6
    },
    ranges: {
      //'Today': [moment(), moment()],
      'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
      'Last 7 Days': [moment().subtract(6, 'days'), moment()],
      'Last 30 Days': [moment().subtract(29, 'days'), moment()],
      'This Month': [moment().startOf('month'), moment().endOf('month')],
      'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
    },
    "alwaysShowCalendars": true,
    "startDate": start,
    "applyButtonClasses": "apply-modif",
    "endDate": end,
    "opens": "left",
    "maxDate": end,
  }, callbackDate);

  callbackDate(start, end, label = "init");
  getDatatoChart(start, end);


  function callbackDate(start, end, label) {
    $('.period .interval,.card-format .big .interval').html(start.format('D MMMM YYYY') + ' - ' + end.format('D MMMM YYYY'));
    console.log('New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')');
    if (label != "init") {
      setTimeout(function () {
        getDatatoChart(start, end);
      }, 50);
    }
  }
  function getDatatoChart(start, end) {
    var result = {};
    result['gross'] = result['gross'] || [];
    result['net'] = result['net'] || [];
    result['avp'] = result['avp'] || [];
    result['upt'] = result['upt'] || [];
    $.ajax({
      'global': false,
      'url': 'data.json?v=' + getRandomInt(1, 10000),
      'dataType': "json",
      'success': function (data) {
        while (start <= end) {

          var temp = data[start.format('D MMMM YYYY')];
          result['gross'].push({
            x: new Date(start.year(), start.month(), start.date()),
            y: typeof (temp) != "undefined" ? parseInt(temp['gross']) : 0
          })
          result['net'].push({
            x: new Date(start.year(), start.month(), start.date()),
            y: typeof (temp) != "undefined" ? parseInt(temp['net']) : 0
          })
          result['avp'].push({
            x: new Date(start.year(), start.month(), start.date()),
            y: typeof (temp) != "undefined" ? parseInt(temp['avp']) : 0
          })
          result['upt'].push({
            x: new Date(start.year(), start.month(), start.date()),
            y: typeof (temp) != "undefined" ? parseInt(temp['upt']) : 0
          })
          start = start.add(1, "days");
        }
        initChart(result);
      }
    });
  }

  function initChart(data) {
    chart = new CanvasJS.Chart("dataGraph", {
      animationEnabled: true,
      theme: "light2",
      axisX: {
        valueFormatString: "D MMMM"
      },
      toolTip: {
        shared: true
      },
      legend: {
        cursor: "pointer",
        itemclick: toggleDataSeries
      },
      data: [
        {
          type: "column",
          name: "Gross",
          color: "#289E45",
          showInLegend: true,
          xValueFormatString: "D MMMM YYYY ",
          dataPoints: data.gross,
        },
        {
          type: "column",
          name: "Nett",
          color: "#37B04C",
          showInLegend: true,
          xValueFormatString: "D MMMM YYYY",
          dataPoints: data.net
        },
        {
          type: "column",
          name: "Average Purchase Value",
          color: "#6BE681",
          showInLegend: true,
          xValueFormatString: "D MMMM YYYY",
          dataPoints: data.avp
        },
        {
          type: "line",
          name: "Gross",
          color: "#FFE854",
          showInLegend: false,
          showInTooltip: false,
          dataPoints: data.gross
        },
        {
          type: "line",
          name: "Unit per Transaction",
          color: "#707070",
          showInLegend: true,
          dataPoints: data.upt
        }
      ]
    });
    chart.render();
  }
  function toggleDataSeries(e) {
    if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
      e.dataSeries.visible = false;
    } else {
      e.dataSeries.visible = true;
    }
    e.chart.render();
  }
  function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
  }
})

$(document).on('click', '.apply-modif', function (event) {
  $('.applyBtn')[0].click();
})


